UNKNOWN_VALUE = 1

class Shape:
    """
    Base class for all shapes.
    """
    pass

class Line(Shape):
    def __init__(self, point1, point2, length=UNKNOWN_VALUE):
        super().__init__()
        self.start = point1
        self.end = point2
        self.length = length

def cal_intersection(line1, line2):
    pass

class Angle(Shape):
    def __init__(self, side1, side2):
        pass

class Triangle(Shape):
    def __init__(
        self, point1, point2, point3,
        length12=UNKNOWN_VALUE, length23=UNKNOWN_VALUE, length13=UNKNOWN_VALUE
    ):
        super().__init__()
        self.side1 = Line(point1, point2, length12)
        self.side2 = Line(point2, point3, length23)
        self.side3 = Line(point1, point3, length13)
