import re


class Theorem:
    """
    Base class for all theorems. Deduce one result from another.
    """
    def __init__(self, condition, conclusion, reversable=False):
        self.condition = condition
        self.conclusion = conclusion
        if reversable:
            self.mirror = Theorem(conclusion, condition) # create a reversed theorem
        else:
            self.mirror = None

    def replace_data(self, statement:str):
        """
        The identifier uses <> to locate key values in the token, and $ is used to identify numeric values.
        when used, the initial data is replaced with data in the question with same patterns.
        TODO: make a function to check patterns.
        """
        s_in_word = statement.split(' ')
        if '<' in s_in_word:
            pass


def check_patterns(standard:str, sample:str):
    """
    This function checks the pattern of the targeted statement.
    It checks the classification of the <value> first, then the fixed pattern.
    :return: boolean
    """
    header1 = standard.split(' ')
    header2 = sample.split(' ')
    if header1 == header2:
        pass # TODO: check fixed ones
    else:
        return False

b1 = Theorem('<scalar a> = <scalar b>, <scalar b> = <scalar c>.', '<scalar a> = <scalar c>.')
# b2 = Theorem()
t1 = Theorem('given <angle AOD> and <angle COB>.', '<angle AOD> = <angle COB>.')
t2 = Theorem('if <line AB> and <line CD> perpendicular at <point O>', '<angle AOC> = $90 degrees', reversable=True)
# TODO: finish this one. it is too long!
t3 = Theorem(
    '<line AB> parrallels with <line CD>, <line EF> intersects <line AB> at <G>, <line CD> at <H>.', 
    '<angle AGE> = <angle CGE>, ...',
    reversable=True
)
t4 = Theorem('<triangle ABC> exists.', '')
